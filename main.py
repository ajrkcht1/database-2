import threading
from database import Database
from manager import Manager
from request import Request

__author__  = 'shachar'
MAX_READERS = 10


def read(manager, to_print = False, debug=False):
    request = Request(Request.read_mode)
    data    = manager.execute_request(request)
    if to_print:
        print data
    if debug:
        print "Reader: " + threading.currentThread().getName()
    return data


def write(manager, key, item):
    request = Request(Request.write_mode, key, item)
    manager.execute_request(request)


def has_alive(threads):
    for thread in threads:
        if thread.is_alive():
            return True
    return False


def wait_all_threads_to_finish(threads):
    while has_alive(threads):
        pass    # Waiting until all threads will finish

#------------Checking-Functions------------


def simple_write(manager):
    threads = []
    thread = threading.Thread(target=write, args=(manager, "Name", "Shachar - simple_write", ))
    threads.append(thread)
    thread.start()
    return threads


def simple_read(manager):
    threads = []
    thread = threading.Thread(target=read, args=(manager, True,))
    threads.append(thread)
    thread.start()
    return threads


def write_during_reading(manager):
    threads = []
    for i in xrange(10000):    # So we will see the delay (since we waiting them to finish)
        thread = threading.Thread(target=read, args=(manager, False, True,))
        threads.append(thread)
        thread.start()
    simple_write(manager)   # It should wait until all readers read
    simple_read(manager)    # In order to check it was writen
    return threads


def read_during_writing(manager):
    threads = []
    for i in xrange(1000):    # So the reading will be while writing
        thread = threading.Thread(target=write, args=(manager, "Name" + str(i), "Shachar - read_during_writing", ))
        threads.append(thread)
        thread.start()
    simple_read(manager)
    return threads


def multiply_read_threads(manager):
    threads = []
    for i in xrange(5000):    # So we will see the delay (since we waiting them to finish)
        thread = threading.Thread(target=read, args=(manager, False, True,))
        threads.append(thread)
        thread.start()
        return threads


def final_check(manager):
    theards  = multiply_read_threads(manager)
    theards += write_during_reading(manager)
    theards += read_during_writing(manager)
    return theards

if __name__ == "__main__":
    filename = raw_input("Please Enter filename: ")
    database = Database(filename)
    manager = Manager(database, MAX_READERS)
    print "------<< Check Number 1 : simple_write >>----------------"
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    wait_all_threads_to_finish(simple_write(manager))
    print "------<< Check Number 2 : simple_read >>----------------"
    wait_all_threads_to_finish(simple_read(manager))
    print "------<< Check Number 3 : write_during_reading  >>------"
    wait_all_threads_to_finish(write_during_reading(manager))
    print "------<< Check Number 4 : read_during_writing   >>------"
    wait_all_threads_to_finish(read_during_writing(manager))
    print "------<< Check Number 5 : multiply_read_threads >>------"
    wait_all_threads_to_finish(multiply_read_threads(manager))
    print "------<< Check Number 6 : final_check >>----------------"
    wait_all_threads_to_finish(final_check(manager))