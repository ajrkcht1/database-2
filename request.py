__author__ = 'Shachar'


class Request(object):
    write_mode = "WRITE"
    read_mode  = "READ"
    type_error = "<ErrorType> type is not 'READ' or 'WRITE'"

    def __init__(self, request_type, key=None, item=None):
        self.type = request_type.upper()
        if self.type == Request.write_mode:
            self.key  = key
            self.item = item
