import win32event
from request import Request

__author__ = 'Shachar'


MAX_READERS      = 10
DEFAULT_REQUESTS = []


class Manager(object):
    writing           = True
    reader_place_size = 1

    def __init__(self, database, requests=DEFAULT_REQUESTS, max_readers=MAX_READERS):
        self.database     = database
        self.requests     = requests[:] if type(requests) is list else [requests]
        self.max_readers  = max_readers
        self.writer_mutex = win32event.CreateMutex(None, False, "writer_mutex")
        self.semaphore    = self.__create_semaphore()

    def __create_semaphore(self):
        return win32event.CreateSemaphore(None, self.max_readers, self.max_readers, "add_request")

    def __wait_for_empty_place_and_grab_it(self, places_to_grab):
        for i in xrange(places_to_grab):
            win32event.WaitForSingleObject(self.semaphore, -1)

    def __release_empty_place(self, places_to_empty):
        for i in xrange(places_to_empty):
            win32event.ReleaseSemaphore(self.semaphore, 1)

    def execute_request(self, request):                         # Or add_request
        self.requests.append(request)
        return self.__execute_request(request)

    def __execute_request(self, request):
        win32event.WaitForSingleObject(self.writer_mutex, -1)   # Acquire
        if request.type == Request.write_mode:
            self.__wait_for_empty_place_and_grab_it(self.max_readers)
            self.database.write(request.key, request.item)
            self.__release_empty_place(self.max_readers)
            win32event.ReleaseMutex(self.writer_mutex)          # Release
        elif request.type == Request.read_mode:
            win32event.ReleaseMutex(self.writer_mutex)          # Release
            self.__wait_for_empty_place_and_grab_it(Manager.reader_place_size)
            data = self.database.read()
            self.__release_empty_place(Manager.reader_place_size)
            return data
        else:
            win32event.ReleaseMutex(self.writer_mutex)          # Release
            raise Exception(Request.type_error)