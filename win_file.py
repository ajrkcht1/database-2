import win32file
import win32con

__author__ = 'Shachar'


class WinFile(object):
    read_mode  = "READ"
    write_mode = "WRITE"
    mode_error = "<ErrorMode> mode is not 'READ' or 'WRITE'"

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode     = mode.upper()
        if self.mode == WinFile.read_mode:
            self.handle = self.__creat_file_to_read()
        elif self.mode == WinFile.write_mode:
            self.handle = self.__creat_file_to_write()
        else:
            raise Exception(WinFile.mode_error)

    def __creat_file_to_read(self):
        handle = win32file.CreateFile(self.filename, win32file.GENERIC_READ, win32file.FILE_SHARE_READ, None, win32con.OPEN_EXISTING, win32file.FILE_ATTRIBUTE_NORMAL, None)

        return handle

    def __creat_file_to_write(self):

        try:
            handle = win32file.CreateFile(self.filename, win32file.GENERIC_WRITE, win32file.FILE_SHARE_WRITE, None, win32con.CREATE_NEW, win32file.FILE_ATTRIBUTE_NORMAL, None)
        except Exception:
            handle = win32file.CreateFile(self.filename, win32file.GENERIC_WRITE, 0, None, win32con.OPEN_EXISTING, 0, None)
        return handle

    def write_file(self, data):
        win32file.WriteFile(self.handle, data)  # Rewrite all data

    def read_file(self):
        rc, data = win32file.ReadFile(self.handle, 1024*10)
        return data

    def close_file(self):
        self.handle.close()


if __name__ == "__main__":
    filename = raw_input("Enter filename: ")

    file_to_write = WinFile(filename, "WRITE")
    file_to_write.write_file("Hello")
    file_to_write.close_file()

    file_to_read = WinFile(filename, "READ")
    print file_to_read.read_file()
    file_to_read.close_file()