from win_file import WinFile
import cPickle

__author__ = 'Shachar'

DEFAULT_DATA  = {}
NO_FILE_ERROR = "<ParameterError> No File Parameter Found"


class Database(object):
    def __init__(self, path_file=NO_FILE_ERROR, data=DEFAULT_DATA):
        self.data = data
        if path_file == NO_FILE_ERROR:
            raise Exception(NO_FILE_ERROR)
        self.path_file = path_file
        #self.win_file_to_read  = WinFile(self.path_file, WinFile.read_mode)    # Not in use
        self.win_file_to_write = WinFile(self.path_file, WinFile.write_mode)
        self.__update_file()

    def read(self):
        return str(self.data)

    def write(self, key, item):
        self.data[key] = item
        self.__update_file()

    def __update_file(self):
        string_data = cPickle.dumps(self.data)
        self.win_file_to_write.write_file(string_data)
